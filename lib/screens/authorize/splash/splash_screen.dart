import 'package:FlutterVer1/theme.dart';
import 'package:flutter/material.dart';
import 'package:FlutterVer1/screens/authorize/splash/components/body.dart';
import 'package:FlutterVer1/size_config.dart';

class SplashScreen extends StatelessWidget {
  static String routeName = "/splash";
  @override
  Widget build(BuildContext context) {
    // You have to call it on your starting screen
    SizeConfig().init(context);
    return Scaffold(
      body: Body(),
      backgroundColor: HexColor.fromHex('FFFFFF'),
    );
  }
}
