import 'package:flutter/material.dart';
import 'package:FlutterVer1/theme.dart';

import 'components/body.dart';

class ProfileDetailScreen extends StatelessWidget {
  static String routeName = "profile_detail";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Body(),
      backgroundColor: HexColor.fromHex('FFFFFF'),
    );
  }
}
