import 'package:flutter/material.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() {
    return _BodyState();
  }
}

class _BodyState extends State<Body> {
  // this allows us to access the TextField text
  TextEditingController textFieldController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final Map<String, Object> rcvdData =
        ModalRoute.of(context).settings.arguments;
    print("rcvd fdata ${rcvdData['name']}");
    print("rcvd fdata ${rcvdData}");

    return Scaffold(
      appBar: AppBar(title: Text("Second")),
      body: Container(
        child: Column(
          children: <Widget>[
            Text("Second"),
          ],
        ),
      ),
    );
  }
}
