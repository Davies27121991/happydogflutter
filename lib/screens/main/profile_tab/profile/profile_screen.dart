import 'package:flutter/material.dart';
import 'package:FlutterVer1/theme.dart';

import 'components/body.dart';

class ProfileScreen extends StatelessWidget {
  static String routeName = "/profile";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Body(),
      backgroundColor: HexColor.fromHex('FFFFFF'),
    );
  }
}
