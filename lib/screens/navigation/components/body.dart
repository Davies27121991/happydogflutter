import 'package:flutter/material.dart';
import 'package:FlutterVer1/screens/main/main_tab/home/home_screen.dart';
import 'package:FlutterVer1/screens/main/message_tab/message/message_screen.dart';
import 'package:FlutterVer1/screens/main/profile_tab/profile/profile_screen.dart';

class Body extends StatefulWidget {
  @override
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  int _currentIndex = 0;
  final List<Widget> _children = [
    HomeScreen(),
    MessageScreen(),
    ProfileScreen(),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('My Flutter App'),
      ),
      body: _children[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        onTap: onTabTapped, // new
        currentIndex: _currentIndex, // new
        items: [
          BottomNavigationBarItem(
            icon: new Icon(Icons.home),
            // ignore: deprecated_member_use
            title: new Text('Home'),
          ),
          BottomNavigationBarItem(
            icon: new Icon(Icons.mail),
            // ignore: deprecated_member_use
            title: new Text('Messages'),
          ),
          BottomNavigationBarItem(
              // ignore: deprecated_member_use
              icon: Icon(Icons.person),
              // ignore: deprecated_member_use
              title: Text('Profile'))
        ],
      ),
    );
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }
}
