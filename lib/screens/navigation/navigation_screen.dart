import 'package:flutter/material.dart';
import 'package:FlutterVer1/theme.dart';

import 'components/body.dart';

class NavigationScreen extends StatelessWidget {
  static String routeName = "/navigation";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Body(),
      backgroundColor: HexColor.fromHex('FFFFFF'),
    );
  }
}
