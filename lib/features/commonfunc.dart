import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class Feature with ChangeNotifier {
  int _count = 0;
  int get count => _count;

  /// Tăng số
  increaseCount() {
    _count++;
    notifyListeners(); // khi thay đổi sẽ lắng nghe và thông báo đến các Widget liên quan
  }
}

showAlertDialog(BuildContext context) {
  // set up the button
  Widget okButton = FlatButton(
    child: Text("OK"),
    onPressed: () {
      Navigator.of(context).pop(); // dismiss dialog
    },
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text("My title"),
    content: Text("This is my message."),
    actions: [
      okButton,
    ],
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}
