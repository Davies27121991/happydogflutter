import 'package:flutter/widgets.dart';
import 'package:FlutterVer1/screens/authorize/splash/splash_screen.dart';
import 'package:FlutterVer1/screens/authorize/sign_in/sign_in_screen.dart';
import 'package:FlutterVer1/screens/authorize/login_success/login_success_screen.dart';
import 'package:FlutterVer1/screens/main/main_tab/home/home_screen.dart';
import 'package:FlutterVer1/screens/main/message_tab/message/message_screen.dart';
import 'package:FlutterVer1/screens/main/profile_tab/profile/profile_screen.dart';
import 'package:FlutterVer1/screens/main/profile_tab/profile_detail/profile_detail_screen.dart';

import 'package:FlutterVer1/screens/navigation/navigation_screen.dart';

// We use name route
// All our routes will be available here
final Map<String, WidgetBuilder> routes = {
  SplashScreen.routeName: (context) => SplashScreen(),
  SignInScreen.routeName: (context) => SignInScreen(),
  LoginSuccessScreen.routeName: (context) => LoginSuccessScreen(),
  HomeScreen.routeName: (context) => HomeScreen(),
  MessageScreen.routeName: (context) => MessageScreen(),
  ProfileDetailScreen.routeName: (context) => ProfileDetailScreen(),
  ProfileScreen.routeName: (context) => ProfileScreen(),
  NavigationScreen.routeName: (context) => NavigationScreen(),
};
