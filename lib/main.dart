import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:FlutterVer1/routes.dart';
import 'package:FlutterVer1/screens/authorize/splash/splash_screen.dart';
import 'package:FlutterVer1/theme.dart';
import 'package:provider/provider.dart';
import 'package:FlutterVer1/features/commonfunc.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => Feature())],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: theme(),
        // home: SplashScreen(),
        // We use routeName so that we dont need to remember the name
        initialRoute: SplashScreen.routeName,
        routes: routesd,
      ),
    );
  }
}
